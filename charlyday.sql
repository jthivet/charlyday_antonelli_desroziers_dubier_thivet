-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 11 Février 2016 à 22:42
-- Version du serveur :  5.1.73
-- Version de PHP :  5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `thivet4u`
--

-- --------------------------------------------------------

--
-- Structure de la table `partenaire`
--

CREATE TABLE IF NOT EXISTS `partenaire` (
  `idUser` int(10) NOT NULL AUTO_INCREMENT,
  `motdepasse` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`idUser`),
  UNIQUE KEY `idClient` (`idUser`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `partenaire`
--

INSERT INTO `partenaire` (`idUser`, `motdepasse`, `username`, `nom`) VALUES
(1, '$2y$12$N/w7QOpNkXRQQoYlYSS7XOv/A/eKadd1eSKWsMznIa8cO8JAJPWAi', 'crazyday', 'IUT Charlemagne'),
(2, '$2y$12$IuTLtpcbZ.ffW/Xbi5GyH.SMzhAQP91V2jlUsldb.iv/B3Qa3zOC6', 'crazyday2', 'IUT Charlemagne v2');

-- --------------------------------------------------------

--
-- Structure de la table `pochette`
--

CREATE TABLE IF NOT EXISTS `pochette` (
  `idPochette` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) DEFAULT NULL,
  `message` text,
  `uniqueId` text NOT NULL,
  `montantdons` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idPochette`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=130 ;

--
-- Contenu de la table `pochette`
--

INSERT INTO `pochette` (`idPochette`, `nom`, `message`, `uniqueId`, `montantdons`) VALUES
(128, 'Ma pochette surprise', 'J&#39;aime le charly day', '56bcfa7fcab1d', 253),
(129, 'Pochette 2', 'Ceci est mon cadeau 2', '56bcfdb482481', 10);

-- --------------------------------------------------------

--
-- Structure de la table `pochetteprestation`
--

CREATE TABLE IF NOT EXISTS `pochetteprestation` (
  `idPochette` int(11) NOT NULL,
  `idPrestation` int(11) NOT NULL,
  `qte` int(11) NOT NULL,
  PRIMARY KEY (`idPochette`,`idPrestation`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pochetteprestation`
--

INSERT INTO `pochetteprestation` (`idPochette`, `idPrestation`, `qte`) VALUES
(128, 5, 1),
(128, 17, 1),
(128, 24, 1),
(128, 28, 1),
(128, 11, 1),
(129, 28, 1),
(129, 17, 1),
(129, 20, 1),
(129, 21, 1);

-- --------------------------------------------------------

--
-- Structure de la table `prestation`
--

CREATE TABLE IF NOT EXISTS `prestation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  `descr` text NOT NULL,
  `type` int(11) NOT NULL,
  `img` text NOT NULL,
  `prix` decimal(5,2) NOT NULL,
  `idPartenaire` int(11) DEFAULT NULL,
  `cumulNotes` int(11) DEFAULT NULL,
  `nbNotes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Contenu de la table `prestation`
--

INSERT INTO `prestation` (`id`, `nom`, `descr`, `type`, `img`, `prix`, `idPartenaire`, `cumulNotes`, `nbNotes`) VALUES
(1, 'Champagne', 'Bouteille de champagne + flutes + jeux à gratter', 1, 'champagne.jpg', '20.00', NULL, NULL, NULL),
(2, 'Musique', 'Partitions de piano à 4 mains', 1, 'musique.jpg', '25.00', NULL, NULL, NULL),
(3, 'Exposition', 'Visite guidée de l’exposition ‘REGARDER’ à la galerie Poirel', 2, 'poirelregarder.jpg', '14.00', NULL, NULL, NULL),
(4, 'Goûter', 'Goûter au FIFNL', 3, 'gouter.jpg', '20.00', NULL, NULL, NULL),
(5, 'Projection', 'Projection courts-métrages au FIFNL', 2, 'film.jpg', '10.00', NULL, NULL, NULL),
(6, 'Bouquet', 'Bouquet de roses et Mots de Marion Renaud', 1, 'rose.jpg', '16.00', NULL, NULL, NULL),
(7, 'Diner Stanislas', 'Diner à La Table du Bon Roi Stanislas (Apéritif /Entrée / Plat / Vin / Dessert / Café / Digestif)', 3, 'bonroi.jpg', '60.00', NULL, NULL, NULL),
(8, 'Origami', 'Baguettes magiques en Origami en buvant un thé', 3, 'origami.jpg', '12.00', NULL, NULL, NULL),
(9, 'Livres', 'Livre bricolage avec petits-enfants + Roman', 1, 'bricolage.jpg', '24.00', NULL, NULL, NULL),
(10, 'Diner  Grand Rue ', 'Diner au Grand’Ru(e) (Apéritif / Entrée / Plat / Vin / Dessert / Café)', 3, 'grandrue.jpg', '59.00', NULL, NULL, NULL),
(11, 'Visite guidée', 'Visite guidée personnalisée de Saint-Epvre jusqu’à Stanislas', 2, 'place.jpg', '11.00', NULL, NULL, NULL),
(12, 'Bijoux', 'Bijoux de manteau + Sous-verre pochette de disque + Lait après-soleil', 1, 'bijoux.jpg', '29.00', NULL, NULL, NULL),
(13, 'Opéra', 'Concert commenté à l’Opéra', 2, 'opera.jpg', '15.00', NULL, NULL, NULL),
(14, 'Thé Hotel de la reine', 'Thé de debriefing au bar de l’Hotel de la reine', 3, 'hotelreine.gif', '5.00', NULL, 40, 12),
(15, 'Jeu connaissance', 'Jeu pour faire connaissance', 2, 'connaissance.jpg', '6.00', NULL, NULL, NULL),
(16, 'Diner', 'Diner (Apéritif / Plat / Vin / Dessert / Café)', 3, 'diner.jpg', '40.00', NULL, NULL, NULL),
(17, 'Cadeaux individuels', 'Cadeaux individuels sur le thème de la soirée', 1, 'cadeaux.jpg', '13.00', NULL, NULL, NULL),
(18, 'Animation', 'Activité animée par un intervenant extérieur', 2, 'animateur.jpg', '9.00', NULL, NULL, NULL),
(19, 'Jeu contacts', 'Jeu pour échange de contacts', 2, 'contact.png', '5.00', NULL, NULL, NULL),
(20, 'Cocktail', 'Cocktail de fin de soirée', 3, 'cocktail.jpg', '12.00', NULL, NULL, NULL),
(21, 'Star Wars', 'Star Wars - Le Réveil de la Force. Séance cinéma 3D', 2, 'starwars.jpg', '12.00', NULL, NULL, NULL),
(22, 'Concert', 'Un concert à Nancy', 2, 'concert.jpg', '17.00', NULL, NULL, NULL),
(23, 'Appart Hotel', 'Appart’hôtel Coeur de Ville, en plein centre-ville', 4, 'apparthotel.jpg', '56.00', NULL, NULL, NULL),
(24, 'Hôtel d''Haussonville', 'Hôtel d''Haussonville, au coeur de la Vieille ville à deux pas de la place Stanislas', 4, 'hotel_haussonville_logo.jpg', '169.00', NULL, NULL, NULL),
(25, 'Boite de nuit', 'Discothèque, Boîte tendance avec des soirées à thème & DJ invités', 2, 'boitedenuit.jpg', '32.00', NULL, NULL, NULL),
(26, 'Planètes Laser', 'Laser game : Gilet électronique et pistolet laser comme matériel, vous voilà équipé.', 2, 'laser.jpg', '15.00', NULL, NULL, NULL),
(27, 'Fort Aventure', 'Découvrez Fort Aventure à Bainville-sur-Madon, un site Accropierre unique en Lorraine ! Des Parcours Acrobatiques pour petits et grands, Jeu Mission Aventure, Crypte de Crapahute, Tyrolienne, Saut à l''élastique inversé, Toboggan géant... et bien plus encore.', 2, 'fort.jpg', '25.00', NULL, NULL, NULL),
(28, 'Visite d&#39;une fabrique de bonbons', 'Vous visiterez une usine de création de sucettes pendant une journée entière.', 2, '56bcfa0abc07f.jpg', '50.00', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `type`
--

INSERT INTO `type` (`id`, `nom`) VALUES
(1, 'Attention'),
(2, 'Activité'),
(3, 'Restauration'),
(4, 'Hébergement');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `prestation`
--
ALTER TABLE `prestation`
  ADD CONSTRAINT `prestation_ibfk_1` FOREIGN KEY (`type`) REFERENCES `type` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
