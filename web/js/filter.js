/**
 * Created by thomas on 11/02/16.
 */
$('#desc').change(function(){
    if(this.checked) {
        if($('#filtre').val()!=0)
            window.location.href = "http://" + window.location.hostname + window.location.pathname + "?desc=true&filtreType="+ $('#filtre').val();
        else
            window.location.href = "http://" + window.location.hostname + window.location.pathname + "?desc=true";
    }else {
        if ($('#filtre').val() != 0)
            window.location.href = "http://" + window.location.hostname + window.location.pathname + "?filtreType=" + $('#filtre').val();
        else
            window.location.href = "http://" + window.location.hostname + window.location.pathname;
    }
});

$('#filtre').change(function(){
    if($('#desc').is(":checked")){
        if(this.value!=0)
            window.location.href = "http://" + window.location.hostname + window.location.pathname + "?desc=true&filtreType="+this.value;
        else
            window.location.href = "http://" + window.location.hostname + window.location.pathname + "?desc=true";
    }else {
        if (this.value != 0)
            window.location.href = "http://" + window.location.hostname + window.location.pathname + "?filtreType=" + this.value;
        else
            window.location.href = "http://" + window.location.hostname + window.location.pathname;
    }
});