<div class = "panel">
	<h2>Connexion</h2>

<?php

$formulaire = "
	<div class = \"panel\">
		<form action=\"" . $app->urlFor('postconnexion') . "\" method=\"post\">
			<div class = \"form-group\">
				<label> Nom d'utilisateur : </label>
				<input class=\"form-control\" type=\"text\" name=\"username\" placeholder=\"Nom d'utilisateur\" />
			</div>
			<div class = \"form-group\">
				<label> Mot de passe : </label>
				<input class=\"form-control\" type=\"password\" name=\"pass\" placeholder=\"Mot de passe\" />
			</div>
			<button class=\"btn btn-primary\" name=\"confirmation\" value=\"connexion\">Se connecter</button>
		</form>
	</div>";
if(!empty($erreurs)){
    foreach ($erreurs as $erreur) {
        echo "<div class=\"alert alert-danger\" role=\"alert\"><p class=\".error\">" . $erreur . "</p></div>";
    }

    echo $formulaire;

} else if(isset($erreurs)){

    echo "<div class=\"alert alert-success\"><p class=\".noerror\"> Vous êtes à présent connecté. <br> Vous serez redirigé vers la page d'accueil dans cinq secondes.
            <br>Cliquez <a href = \"" . $app->urlFor('home') . "\"> ici </a> pour être redirigé manuellement. </p></div>";
    ?>
    <script>
        window.setTimeout(function(){
                window.location = "<?= $app->urlFor('home'); ?>";
            }
            ,5000);
    </script>

<?php
} else{

    echo $formulaire;

}

?>
</div>