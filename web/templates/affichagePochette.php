<div class = "panel">
<?php

use Models\Prestation as Prestation;
use Models\Pochette as Pochette;

if(isset($erreurs) && !empty($erreurs)){
    foreach ($erreurs as $erreur) {
       echo '
			<div class="alert alert-danger" role="alert">
				<h3>Vous avez commis des erreurs :</h3>';
            foreach ($erreurs as $erreur) {
                echo "<p>" . $erreur . "</p>";
            }
            echo '</div>';
    }
} else{
    $html='
	<h1> Contenu de votre pochette :</h1>';
    if (!isset($_SESSION['uniqueId'])) {
        $html.='Erreur: Pochette introuvable dans la base.';
    }else {
        $Pochette= Pochette::where('uniqueId',$_SESSION['uniqueId'])->first();
        $Prestations= $Pochette->prestations;
        $Prix_a_payer=0;
        $NbTypes= array();
        if($Prestations != NULL) {
            foreach ($Prestations as $value) {
                if (!in_array($value->ptype->nom, $NbTypes)) {
                    array_push($NbTypes, $value->ptype->nom);
                }
            }
        }
		if(count($NbTypes)<3){
            $html.='
			<div class="alert alert-danger" role="alert"><p>Votre pochette comprend moins de 3 types de prestations</p></div>';
        }
		$html.='
		<div class = "panel row">
		<p>Nom de votre prochette : '.$Pochette->nom.'</p>
		<p>Message de votre prochette : '.$Pochette->message.'</p>';

        if($Prestations != NULL && count($Prestations)>0){
            $html .= '<div class="table-responsive">';
            $html .= '<table class="table">';
            $html .= '<tr><th>Prestations</th><th>Type prestation</th><th>prix</th><th>Quantité</th></tr>';
            foreach ($NbTypes as $valueII) {
                foreach ($Prestations as $value) {
                    if ($value->ptype->nom == $valueII) {
                        $html .= '<tr>';
                        $html .= '<td>' . $value->nom . '</td>';
                        $html .= '<td>' . $value->ptype->nom . '</td>';
                        $html .= '<td>' . $value->prix . '</td>';
                        $html .= '<td>' . $value->pivot->qte . '</td>';
                        $Prix_a_payer += ($value->prix * $value->pivot->qte);
                    }
                }
            }
            $html .= '</table>';
            $html .= '</div>';
        }
		$html.="<p>Montant total a régler: $Prix_a_payer €</p>";
        if(count($NbTypes)>=3){
            $html.= "<h2>Vous pouvez dès à partager le lien vers la page de donation : </h2>";
            $lien = "http://".$app->request->getHost().$app->urlFor('donationpochette',array('id'=>$_SESSION['uniqueId'] ));
			$html.= "<a href=\"$lien\">$lien</a>";
			$html.= "<p></p>";
        }
    }
    echo $html;
}

?>
</div>
</div>