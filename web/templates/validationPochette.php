<?php

if(isset($erreur) && !empty($erreurs)){
	foreach ($erreurs as $erreur) {
		echo "<p>" . $erreur . "</p>";
	}
} else{
	$html='
	<div class = "panel row">
		<div class="alert alert-success">
			<p>Votre pochette a bien été créée</p>
		</div>';
	if (!isset($_SESSION['uniqueId']) || $_SESSION['uniqueId']==null){
		$html .='Erreur: Pochette introuvable dans la base.';
	}else {
		$html .='<a class="btn btn-primary" href="' . $app->urlFor('listeprestations') . '"> Retour a la liste des prestations</a >
		<a class="btn btn-primary" href="' . $app->urlFor('afficherpochette') . '"> Afficher Pochette</a >';
	}
	$html .='</div>';
	echo $html;
}

?>