<?php


use Models\Pochette as Pochette;

$html ='<div class="panel">';
$html.='<h1> Cadeau </h1>';
if (!isset($id)) {
    $html.='Erreur: Pochette introuvable dans la base.';
}else {
    $Pochette= Pochette::where('uniqueId',$id)->first();
    $html.='<div class="row">';
    $html.='<h4 class="col-md-2 col-xs-2">Nom:'. $Pochette->nom. '</h4>';
    $html.='<h4 class="col-md-2 col-xs-2">Message:'.$Pochette->message.'</h4>';
    $html.='</div>';
    $html.='<div class="row">';
    $Prestations= $Pochette->prestations;
    $NbTypes= array();
    if($Prestations != NULL) {
        foreach ($Prestations as $value) {
            if (!in_array($value->ptype->nom, $NbTypes)) {
                array_push($NbTypes, $value->ptype->nom);
            }
        }
    }
    $html.='<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="6000">';
    $html.='<ol class="carousel-indicators">';
    if(count($Prestations)>0){
        $html .= '<li data-target="#myCarousel" data-slide-to="0" class="active"></li>';
    }
    for($i=1 ; $i < count($Prestations) ;$i++)
    {
        $html .= '<li data-target="#myCarousel" data-slide-to="'.$i.'"></li>';
    }
    $html.='</ol>';

    $html.='<div class="carousel-inner" role="listbox">';
    if(count($Prestations)>0) {
        $html .= '<div class="item active">';
        $html .= '<img src="' . URL_FIXE . 'web/img/' . $Prestations[0]->img . '" width="460" height="345">';
        $html .= '<div class="carousel-caption"><h3 style="color:black">' . $Prestations[0]->nom . '</h3></div>';
        $html .= '</div>';
    }

    for($i=1 ;$i < count($Prestations);$i++)
    {
                $html.='<div class="item">';
                $html.='<img src="'.URL_FIXE.'web/img/'.$Prestations[$i]->img.'" width="460" height="345">';
                $html.='<div class="carousel-caption"><h3 style="color:black">'.$Prestations[$i]->nom.'</h3></div>';
                $html.='</div>';
                //$html .= "<p>$value->nom qte:" . $value->pivot->qte . "</p>";
    }
    $html.='</div>';
    $html.='
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    ';
    $html.='</div>';
    $html.='</div>';
    $html.='</div>';
    $html.='</div>';
}
echo $html;


?>