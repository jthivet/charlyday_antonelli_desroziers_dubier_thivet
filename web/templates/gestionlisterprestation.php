<?php
/**
 * Created by PhpStorm.
 * User: thivet4u
 * Date: 11/02/2016
 * Time: 16:00
 */
if(!empty($prestations)&&$prestations!=null&&count($prestations)>0){
	echo '<div class="panel">
			<div class="row">
				<a class="btn btn-primary col-md-6 col-xs-6 col-md-offset-3 col-xs-offset-3" href="'.$app->urlFor('gestionajouter').'">Ajouter une prestation</a>
			</div>
			<br>
			<div class="row">
			<a class="btn btn-primary col-md-6 col-xs-6 col-md-offset-3 col-xs-offset-3" href="'.$app->urlFor('gestionlistercommandes').'">Afficher les pochettes où mes prestations sont utilisées</a>
			</div>
			<h1>Le(s) prestation(s) que vous avez ajoutée(s) :</h1>';
	foreach($prestations as $prestation) {
		$html='
		<div class="panel">
			<div class="row">
				<img class="col-md-3 col-xs-3" src="'.URL_FIXE.'web/img/'.$prestation->img.'" class="img-responsive"/>
				<div class="col-md-6 col-xs-6">
					<h2>'.$prestation->nom.'</h2>
					<h2> Prix : '.$prestation->prix.' €</h2>
					<a class="btn btn-primary" href="'.$app->urlFor('detailprestation',array('id' => $prestation->id)).'">Details</a >
					<a class="btn btn-primary" href="'.$app->urlFor('gestionmodifier',array('id' => $prestation->id)).'">Modifier</a >
					<a class="btn btn-primary" href="'.$app->urlFor('gestiondelete',array('id' => $prestation->id)).'">Supprimer</a >
				</div>
			</div>
		</div>';
		echo $html;
	}
	echo '
		</div >
	</div>';
} else {
	echo '<div class="panel">
		<h1>Vous n\'avez pas de prestation :-(</h1>
		<h2><a href="'.$app->urlFor('gestionajouter').'">En ajouter une !</a></h2>
		</div>';
	
}

?>