<div class="panel panel-default">
	<div class="row">
		<center><h2> Inscription </h2></center>

	<?php
		if(!empty($erreurs)){
			echo '
			<div class="alert alert-danger" role="alert">
				<h3>Vous avez commis des erreurs :</h3>';
			foreach ($erreurs as $erreur) {
				echo "<p>" . $erreur . "</p>";
			}
			echo '</div>';
		} else if(isset($erreurs)){
			echo '
		<div class="alert alert-success">
			<p> Merci de votre inscription. </p>
		</div>';
		}

	?>

		<div class="panel">
			<form action="<?= $app->urlFor('postinscription') ?>" method="post">
				<div class = "form-group">
					<label> Nom d'utilisateur : </label>
					<input type="text" class="form-control" name="username" placeholder="Jean_Bomber" />
				</div>
				<div class = "form-group">
					<label> Nom de partenaire : </label>
					<input type="text" class="form-control" name="nom" placeholder="Walygastrumf" />
				</div>
				<div class = "form-group">
					<label> Mot de passe : </label>
					<input type="password" class="form-control" name="pass" placeholder="**********" />
				</div>
				<div class = "form-group">
					<label"> Répétez le mot de passe : </label>
					<input type="password" class="form-control" name="repass" placeholder="**********" />
				</div>
				<button class="btn btn-primary" name="confirmation" value="Valider">Valider</button>
			</form>
		</div>
	</div>
</div>