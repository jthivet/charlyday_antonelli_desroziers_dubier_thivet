<?php
	echo '
		<nav class="navbar navbar-default ">
			<div class="navbar navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="'.$app->urlFor('home').'">Accueil</a></li>
					<li><a>-</a></li>
					<li><a href="'.$app->urlFor('listeprestations',array("p"=>1)).'">Liste des prestations</a></li>
					<li><a>-</a></li>
					<li><a href="'.$app->urlFor('creerpochette').'">Créer pochette</a></li>
					<li><a>-</a></li>';				
					if(empty($_SESSION['partenaire']))
						echo '
					<li><a href="'.$app->urlFor('inscription').'">Créer un compte partneaire</a></li>
					<li><a>-</a></li>
					<li><a href="'.$app->urlFor('connexion').'">Connexion</a></li>';
					else
						echo '<li><a href="'.$app->urlFor('gestionlisteprestations').'">Gérer vos prestations</a></li>
					<li><a>-</a></li>
					<li><a href="'.$app->urlFor('deconnexion').'">Deconnexion</a>	</li>';
					echo'
				</ul>		
			</div>
		</nav>';
?>