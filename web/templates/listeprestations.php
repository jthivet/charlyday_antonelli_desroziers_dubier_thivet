<?php
echo '<div class="panel">
		<h1> Liste des prestations :</h1>';
$panelFiltre = '
	<div class = "panel panel-filtre">
    <div class="row row-centered">
	<div class="col-md-1 col-xs-1 col-centered">Type :</div>
		<div class="col-md-4 col-xs-4 col-centered">
		<select id="filtre" name="filtreType" class="form-control">';
    $panelFiltre.='<option value="0">...</option>';

    foreach($types as $type) {
        if($filtreType == $type->id)
            $panelFiltre .= '<option value="' . $type->id . '" selected>' . $type->nom . '</option>';
        else
            $panelFiltre .= '<option value="' . $type->id . '">' . $type->nom . '</option>';
    }
    $panelFiltre.=
    '	</select>
		</div>
        <div class="checkbox col-md-2 col-xs-2 col-centered">
            <label><input id="desc" type="checkbox" name="prix" value="DESC"';
    if($desc)
        $panelFiltre.='checked="true"';

    $panelFiltre.=
        '>Prix décroissant</label>
        </div>
	</div>
	</div>';
echo $panelFiltre;

foreach($prestations as $prestation) {
    $Note=" -";
    if($prestation->nbNotes != null) {
        $Note = round($prestation->cumulNotes / $prestation->nbNotes,2);
    }
    $html='
    <div class="panel">
        <div class="row">
            <img class="col-md-3 col-xs-3" src="'.URL_FIXE.'web/img/'.$prestation->img.'" class="img-responsive"/>
			<div class="col-md-6 col-xs-6">
				<h2>'.$prestation->nom.'</h2>
				<h2> Prix : '.$prestation->prix.' €</h2>';
				if($prestation->idPartenaire!=null)$html.='<p>Ce service vous est offert par : '.$prestation->partenaire->nom.'</p>';
				$html.='
			</div>
        </div>
		<div class="row">
			<div class="col-md-3 col-xs-3 ">
				<form class="form" action="'.$app->urlFor('ajouterapochette').'" method="post">
					<button class="btn btn-primary" name="idPrestation" value="'. $prestation->id .'"> Ajouter a la pochette </button >
				</form>
			</div>
			<div class="col-md-3 col-xs-3 col-md-offset-6 col-xs-offset-6">
				<a class="btn btn-primary" href="'.$app->urlFor('detailprestation',array('id' => $prestation->id)).'"> Details</a >
			</div>
			<p>Note Moyenne des utilisateurs:'.$Note.' </p>
		</div>	
    </div >';
    echo $html;
}
echo '<ul class="pagination">';
$filter = '';
if($filtreType!=0) {
    $filter = '?filterType=' . $filtreType;
    if ($desc)
        $filter .= '&desc=' . $desc;
}else if($desc)
    $filter = '?desc='.$desc;

for($i=1; $i < $pagination;$i++)
{
    if($p == $i)
        echo '<li class="active"><a href="'.$app->urlFor('listeprestations',array('p'=> $i)).$filter.'">'.$i.'</a></li>';
    else
        echo '<li><a href="'.$app->urlFor('listeprestations',array('p'=> $i)).$filter.'">'.$i.'</a></li>';
}
echo '</ul>';
echo '</div>';
?>