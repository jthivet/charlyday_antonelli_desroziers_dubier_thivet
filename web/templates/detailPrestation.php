<?php
	$Note=" -";
	if($prestation->nbNotes != null) {
		$Note = round($prestation->cumulNotes / $prestation->nbNotes,2);
	}
    $html = '
    <div class="panel panel-default">
		<div class="row">
			<img class="col-md-6 col-xs-6" src="'.URL_FIXE.'web/img/'.$prestation->img.'" class="thumbnail img-responsive img-display text-center">
			<div class="col-md-6 col-xs-6">
				<h3>Nom : '.$prestation->nom.'</h3>
				<h3>Prix : '.$prestation->prix.' €</h3>
				<h3>'.$prestation->descr.'<h3>
				<div class="row">
					<div class="col-md-6 col-xs-6">
						<form class="form" action="'.$app->urlFor('ajouterapochette').'" method="post">
							<button class="btn btn-primary" name="idPrestation" value="'. $prestation->id .'"> Ajouter a la pochette </button >
						</form>
					</div>
					<div class="col-md-6 col-xs-6">
						<a class="btn btn-primary" href="'.$app->urlFor('listeprestations').'">Retour</a>
					</div>
					<p>Note Moyenne des utilisateurs:'.$Note.' </p>
					<form class="form" action="'.$app->urlFor('voterprestation').'" method="post">
						<div class = "form-group">
						<label>Noter: </label>
						<input class="form-control" type="number" name="note" placeholder="note/5">
					</div>
				<button class="btn btn-primary" name="idPrestation" value="'.$prestation->id.'"> Voter Prestation</button ></form>
				</div>
			</div>
		</div>
    </div>';
    echo $html;
?>