<div class="panel panel-default">
	<div class="row">
	    <h2 style="text-align:center"> Ajouter une prestation </h2>

	<?php
		if(!empty($erreurs)){
			echo '
			<div class="alert alert-danger" role="alert">
				<h3>Vous avez commis des erreurs :</h3>';
            foreach ($erreurs as $erreur) {
                echo "<p>" . $erreur . "</p>";
            }
			echo '</div>';
        } else if(isset($erreurs)){
            echo '
		<div class="alert alert-success">
			<p> Votre prestation a bien été ajoutée. </p>
		</div>';
        }

	?>

<?php

$listetypes = '';
$types = \Models\Type::all();

foreach($types as $type){
    $listetypes .= '<option value="' . $type['id'] . '">' . $type['nom'] . '</option>';
}


?>


<div class="panel">
    <form action="<?= $app->urlFor('gestionpostajouter') ?>" method="post" enctype="multipart/form-data">
        <div class = "form-group">
            <label> Nom de la prestation : </label>
            <input type="text" class="form-control" name="prestname" placeholder="Repas romantique" />
        </div>
        <div class = "form-group">
            <label> Description : </label>
            <textarea name="description" class="form-control"></textarea>
        </div>
        <div class = "form-group">
            <label> Type : </label>
            <select name="type"> <?= $listetypes ?> </select>
        </div>
        <div class = "form-group">
            <label> Prix : </label>
            <input type="number" class="form-control" name="prix" placeholder="10" />
        </div>
        <div class = "form-group">
            <label> Image associée : </label>
            <input type="file" name="image" />
        </div>
        <button class="btn btn-primary" name="confirmation" value="Valider">Valider</button>
    </form>
</div>
</div>
</div>