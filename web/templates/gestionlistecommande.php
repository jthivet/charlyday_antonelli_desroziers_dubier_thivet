<?php
/**
 * Created by PhpStorm.
 * User: thivet4u
 * Date: 11/02/2016
 * Time: 16:00
 */

if(count($pochettes)>0){
    echo '<div class="panel">
			<h3>Vos prestations convoitées :</h3>
			<div class="row">
            <div class="col-md-6 col-xs-6">Voici les pochettes dans lesquelles vos prestations sont utilisées :</div></div>';

    $html = '';
    foreach($pochettes as $pochette) {
        $prestations = $pochette->prestations;
        $Prix_a_payer=0;

        foreach($prestations as $value) {
            $Prix_a_payer += ($value->prix * $value->pivot->qte);
        }


        $html .='
            <div class="panel">
            <div class="row">
            <div class="col-md-4 col-xs-4"><h4>Nom : '. $pochette->nom .'</h4></div>
            </div>
            <div class="row">
            <div class="col-md-4 col-xs-4">État de la cagnotte : '. $pochette->montantdons .' / ' . $Prix_a_payer . '</div>
            </div></div>
            ';

    }
    echo $html;
    echo '
		</div >
	</div>';
} else {
    echo '<div class="panel">
		<h1>Aucunes de vos prestations ne sont convoitées pour le moment.</h1>
		</div>';

}

?>