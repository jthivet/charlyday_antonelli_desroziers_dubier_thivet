<?php


use Models\Pochette as Pochette;

    $html='
	<div class = "panel row">
	<h1> Page de Donation </h1>
	<div class = "panel row">';
    if (!isset($id)) {
        $html.='Erreur: Pochette introuvable dans la base.';
    }else {
        $Pochette= Pochette::where('uniqueId',$id)->first();
        $html.="<p>Nom: $Pochette->nom</p>";
        $html.="<p>Message: $Pochette->message</p>";
        $Prestations= $Pochette->prestations;
        $Prix_a_payer=0;
        $NbTypes= array();
        if($Prestations != NULL) {
            foreach ($Prestations as $value) {
                if (!in_array($value->ptype->nom, $NbTypes)) {
                    array_push($NbTypes, $value->ptype->nom);
                }
            }
        }

        $html .= '<div class="table-responsive">';
            $html .= '<table class="table">';
            $html .= '<tr><th>Prestations</th><th>Type prestation</th><th>prix</th><th>Quantité</th></tr>';
            foreach ($NbTypes as $valueII) {
                foreach ($Prestations as $value) {
                    if ($value->ptype->nom == $valueII) {
                        $html .= '<tr>';
                        $html .= '<td>' . $value->nom . '</td>';
                        $html .= '<td>' . $value->ptype->nom . '</td>';
                        $html .= '<td>' . $value->prix . '</td>';
                        $html .= '<td>' . $value->pivot->qte . '</td>';
                        $Prix_a_payer += ($value->prix * $value->pivot->qte);
                    }
                }
            }
            $html .= '</table>';
            $html .= '</div></br>';
        $finalPrix=($Prix_a_payer-$Pochette->montantdons);
        if($finalPrix>0){
            $html.="<p>Montant total a régler: $Prix_a_payer €</p>";
            $html.="<p>Montant restant à collecter: ".$finalPrix." €</p>";
            $html.='<form class="form" action="'.$app->urlFor('ajoutdonationpochette').'" method="post">
				<div class = "form-group row">
					<div class ="col-md-9 col-xs-9">
						<label>Dons: </label>
					</div>
					<div class ="col-md-3 col-xs-3">
						<input class="form-control" type="number" name="don" placeholder="Votre montant en €">
					</div>
				</div>
		        <button class="btn btn-primary" name="idPochette" value="'.$id.'"> Valider Dons</button ></form>';
        }else{
            $html.="<div class=\"alert alert-success\"><p>Bravo! Les dons ont été complétés!</p></div>";
            $html.="Voici l'url à envoyer à ton proche: ";
            $cadeau = 'http://'.$app->request->getHost().$app->urlFor('liencadeau',array('id'=>$id ));
            $html.= '<a href="'.$cadeau.'">'.$cadeau.'</a>';
        }
    }
	$html.="</div></div>";
    echo $html;


?>