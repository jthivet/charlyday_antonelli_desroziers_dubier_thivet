<?php
use Models\Prestation as Prestation;

$PrestationsAttention= Prestation::Where('type', 1)->get();
$PrestationsActivite= Prestation::Where('type', 2)->get();
$PrestationsRestauration= Prestation::Where('type', 3)->get();
$PrestationsHebergement= Prestation::Where('type', 4)->get();
$currentNote=0;
$idTopAttention=1;
foreach($PrestationsAttention as $value){
	if($value->nbNotes != null){
		if($value->cumulNotes/$value->nbNotes>$currentNote){
			$currentNote=$value->cumulNotes/$value->nbNotes;
			$idTopAttention=$value->id;
		}
	}
}
$currentNote=0;
$idTopActivite=3;
foreach($PrestationsActivite as $value){
	if($value->nbNotes != null){
		if($value->cumulNotes/$value->nbNotes>$currentNote){
			$currentNote=$value->cumulNotes/$value->nbNotes;
			$idTopActivite=$value->id;
		}
	}
}
$currentNote=0;
$idTopRestauration=4;
foreach($PrestationsRestauration as $value){
	if($value->nbNotes != null){
		if($value->cumulNotes/$value->nbNotes>$currentNote) {
			$currentNote = $value->cumulNotes / $value->nbNotes;
			$idTopRestauration = $value->id;
		}
	}
}
$currentNote=0;
$idTopHebergement=23;
foreach($PrestationsHebergement as $value){
	if($value->nbNotes != null){
		if($value->cumulNotes/$value->nbNotes>$currentNote){
			$currentNote=$value->cumulNotes/$value->nbNotes;
			$idTopHebergement=$value->id;
		}
	}
}
$TopAttention = Prestation::find($idTopAttention);
$TopActivite = Prestation::find($idTopActivite);
$TopRestauration = Prestation::find($idTopRestauration);
$TopHebergement = Prestation::find($idTopHebergement);
$html='
	<div class="container">
		<div class = "panel">
			<hr>
			<div class="row">
				<div class="col-sm-8">
					<h2>Crazy Charly Day</h2>
					<p>Créez votre propre pochette personnalisé contentant différentes activités originales pour vous ou pour offrir.</p>
					<p>
						<a class="btn btn-default btn-lg" href="'.$app->urlFor('creerpochette').'">Créer votre propre pochette</a>
					</p>
				</div>
				<div class="col-sm-4">
					<h2>Notre équipe :</h2>
					<ul>
						<li>ANTONELLI Lucas</li>
						<li>DUBIER Thomas</li>
						<li>DESROZIERS Jérome</li>
						<li>THIVET Jérémy</li>
					</ul>
				</div>
			</div>
			<hr>
			<h1> Les meilleures prestations :</h1>
			<div class="row">
				<div class="col-sm-6">
					<div class="panel">
						<h1>Top Attention</h1>
						<img class="img-circle img-responsive img-center"  src="'.URL_FIXE.'web/img/'.$TopAttention->img.'" alt="">
						<div class="col-md-3 col-xs-3">
				            <a class="btn btn-primary" href="'.$app->urlFor('detailprestation',array('id' => $TopAttention->id)).'"> Details</a >
			            </div>
						<div class="col-md-offset-8 col-xs-offset-8">
							<p>'.$TopAttention->nom.'</p>
						</div>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="panel">
						<h1>Top Activite</h1>
						<img class="img-circle img-responsive img-center" src="'.URL_FIXE.'web/img/'.$TopActivite->img.'" alt="">
						<div class="col-md-3 col-xs-3">
				            <a class="btn btn-primary" href="'.$app->urlFor('detailprestation',array('id' => $TopActivite->id)).'"> Details</a >
			            </div>
						<div class="col-md-offset-8 col-xs-offset-8">
							<p>'.$TopActivite->nom.'</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="panel">
						<h1>Top Restauration</h1>
						<img class="img-circle img-responsive img-center" src="'.URL_FIXE.'web/img/'.$TopRestauration->img.'" alt="">
						<div class="row">
							<div class="col-md-3 col-xs-3">
								<a class="btn btn-primary" href="'.$app->urlFor('detailprestation',array('id' => $TopRestauration->id)).'"> Details</a >
							</div>
							<div class="col-md-offset-8 col-xs-offset-8">
								<p>'.$TopRestauration->nom.'</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel">
						<h1>Top Hebergement</h1>
						<img class="img-circle img-responsive img-center" src="'.URL_FIXE.'web/img/'.$TopHebergement->img.'" alt="">
						<div class="col-md-3 col-xs-3">
				            <a class="btn btn-primary" href="'.$app->urlFor('detailprestation',array('id' => $TopHebergement->id)).'"> Details</a >
			            </div>
						<div class="col-md-offset-8 col-xs-offset-8">
							<p>'.$TopHebergement->nom.'</p>
						</div>
					</div>
				</div>
			</div>
			<hr>
		</div>
	</div>';
echo $html;

?>