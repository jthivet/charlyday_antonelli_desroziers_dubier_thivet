<?php
/**
 * Created by PhpStorm.
 * User: thivet4u
 * Date: 11/02/2016
 * Time: 16:00
 */

$updateok = false;
$html='
<div class="panel">
<div class="row">
<h2>Modifier votre prestation</h2>

<p>Laissez les champs vides pour laisser la valeur d\'origine.</p>';
echo $html;

		if(!empty($erreurs)){
            echo '
			<div class="alert alert-danger" role="alert">
				<h3>Vous avez commis des erreurs :</h3>';
            foreach ($erreurs as $erreur) {
                echo "<p>" . $erreur . "</p>";
            }
            echo '</div>';
        } else if(isset($erreurs)){
            $updateok = true;
            echo '
		<div class="alert alert-success">
			<p> Votre prestation a bien été modifiée. </p>
			<a class="btn btn-primary" href="'.$app->urlFor('gestionlisteprestations').'">Retour à vos prestations</a >
		</div>';
        }

if(!$updateok){
$listetypes = '';
$types = \Models\Type::all();

foreach($types as $type){
    if($type['id'] != $prestation->type)
        $listetypes .= '<option value="' . $type['id'] . '">' . $type['nom'] . '</option>';
    else
        $listetypes .= '<option value="' . $type['id'] . '" selected="selected">' . $type['nom'] . '</option>';
}



?>

<form action="<?= $app->urlFor('gestionpostmodifier', array('id' => $prestation->id)) ?>" method="post" enctype="multipart/form-data">
    <div class = "form-group">
        <label> Nom de la prestation : </label>
        <input type="text" class="form-control" name="prestname" placeholder="<?= $prestation->nom  ?>" />
    </div>
    <div class = "form-group">
        <label> Description : </label>
        <textarea name="description" class="form-control"><?= $prestation->descr  ?></textarea>
    </div>
    <div class = "form-group">
        <label> Type : </label>
        <select name="type"> <?= $listetypes ?> </select>
    </div>
    <div class = "form-group">
        <label> Prix : </label>
        <input type="number" class="form-control" name="prix" placeholder="<?= $prestation->prix  ?>" />
    </div>
    <div class = "form-group">
        <label> Image associée : </label>
        <img class="col-md-3 col-xs-3" src="<?= URL_FIXE . 'web/img/' . $prestation->img  ?>" class="img-responsive"/>
        <input type="file" name="image" />
    </div>
    <button class="btn btn-primary" name="confirmation" value="Valider">Valider</button>
</form>

</div>
</div>

<?php
}
?>
