<?php
/**
 * Created by PhpStorm.
 * User: thivet4u
 * Date: 11/02/2016
 * Time: 16:00
 */

$html='
<div class="panel">

<h2>Supprimer votre prestation</h2>

<p>Êtes-vous sûr de vouloir supprimer votre prestation :</p>
<p>• ' . $prestation->nom . '</p>

<form method="post" action="' .$app->urlFor('gestionpostdelete', array('id' => $prestation->id)) .'">
    <input type="hidden" name="idprestation" value=' . $prestation->id . ' />

    <button class="btn btn-primary" name="confirmation" value="Confirmer la suppression">Valider</button>
</form>
<br>
<a class="btn btn-primary" href="'.$app->urlFor('gestionlisteprestations').'">Retour à vos prestations</a >

</div>';


echo $html;

