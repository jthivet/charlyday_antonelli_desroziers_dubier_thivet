<?php
/**
  * File:  index.php
  * Auteurs : Jérémy
  * Description : Index regroupant les routes et les différentes instanciation.
  */

// Require autoloader.
require_once 'vendor/autoload.php';

session_start();
// require des constantes

require_once __DIR__."/src/Config/constantes.php";

// use
use \Config\DbConf;
use \Controllers\Router;
use \Controllers\AbstractController;
use \Controllers\PublicController;
use \Controllers\AuthentificationController;

// initialisation de la BDD
DbConf::Init('src/Config/db.conf.ini'); 


// Initialisation de Slim
$configuration = [
    'settings' => [
        'displayErrorDetails' => true
    ],
    'templates.path' => 'web/templates'
];

$app = new \Slim\Slim($configuration);


// Création du routeur
 $router = new Router($app);


/***************************************************************************************/

// Création des routes


 $router->get('/', 'Public@home')->name('home');

 // Inscription
 $router->get('/inscriptionpartenaire', 'Authentification@inscription')->name('inscription');
 $router->post('/inscriptionpartenaire/submit', 'Authentification@postinscription')->name('postinscription');
$router->get('/gererprestations/lister','Gestion@listerprestations')->name('gestionlisteprestations');
$router->get('/gererprestations/modifier/:id','Gestion@modifier')->name('gestionmodifier');
$router->post('/gererprestations/modifier/:id/submit','Gestion@postmodifier')->name('gestionpostmodifier');
$router->get('/gererprestations/delete/:id','Gestion@delete')->name('gestiondelete');
$router->post('/gererprestations/delete/:id/submit','Gestion@postdelete')->name('gestionpostdelete');
$router->get('/gererprestations/ajouter','Gestion@ajouter')->name('gestionajouter');
$router->post('/gererprestations/ajouter/submit','Gestion@postajouter')->name('gestionpostajouter');
$router->get('/gerercommandes/liste','Gestion@listercommandes')->name('gestionlistercommandes');
$router->get('/connexionpartenaire','Authentification@connexion')->name('connexion');
$router->post('/connexionpartenaire/submit','Authentification@postconnexion')->name('postconnexion');
$router->get('/deconnexion','Authentification@deconnexion')->name('deconnexion');
 // Afficher Liste Prestations
$router->get('/listeprestations/:p', 'Prestations@afficherPrestations')->name('listeprestations');
$router->get('/prestation/:id', 'Prestations@afficherPrestation')->name('detailprestation');
$router->post('/prestation/vote','Prestations@voterPrestation')->name('voterprestation');
 // Affichage Pochette
 /************************************************************************************************/
$router->get('/pochettesurprise', 'Pochette@creerPochette')->name('creerpochette');
$router->post('/pochettesurprise/validerpochette', 'Pochette@validerPochette')->name('validerpochette');
$router->get('/pochettesuprise','Pochette@afficherPochette')->name('afficherpochette');
$router->get('/pochettesuprise/:id','Pochette@donationPochette')->name('donationpochette');
$router->post('/pochettesuprise','Pochette@ajoutDonationPochette')->name('ajoutdonationpochette');
$router->post('/pochettesuprise/ajouterprestation','Pochette@ajouterPrestation')->name('ajouterapochette');
$router->get('/pochettesuprise/liencadeau/:id','Pochette@lienCadeau')->name('liencadeau');
 /************************************************************************************************/

/***************************************************************************************/
// Lancement de l'app et gestion des layouts globaux.
 $app->render('layouts/headers.php', ['app' => $app]);
 $app->render('layouts/navbar.php', ['app' => $app]);
 $app->run();
 $app->render('layouts/footer.php', ['app' => $app]);
 

