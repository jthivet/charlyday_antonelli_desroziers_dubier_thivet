<?php
/**
 * File:  DbConf.php
 * Creation Date: 05/01/2016
 * Description: Fabrique la connexion à la base de données à partir d'un fichier .ini.
 *
 * @author: Jérémy Thivet
 */
 
 namespace Config;

 use Illuminate\Database\Capsule\Manager as DB;
 
 class DbConf {
	 
	 public static function Init($file){
		 
			$db =  new DB();
			$db->addConnection(parse_ini_file($file));
			
			$db->setAsGlobal();
			$db->bootEloquent();
		 
	 }
	 
 }