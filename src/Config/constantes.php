<?php
/**
 * File:  constantes.php
 * Creation Date: 11/02/2016
 * Description: Définitions de constantes pour l'application
 *
 * @author: Jérémy
 */ 
 
 
 /**
  * Constante de l'adresse absolue du site web
  */
  
  define("URL_FIXE", substr($_SERVER['SCRIPT_NAME'],0,-9));
