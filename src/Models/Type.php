<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 11/02/16
 * Time: 09:46
 */

namespace Models;


use Illuminate\Database\Eloquent\Model;

class Type extends Model
{

    protected  $table='type';
    protected  $primaryKey='id';
    public $timestamps=false;

    public function prestations(){
        return $this->hasMany('\Models\Prestation','type');
    }
}