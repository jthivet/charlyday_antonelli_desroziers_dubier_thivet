<?php

/**
 * File:  Partenaire.php
 * Creation Date: 11/02/2016
 * Description: Représente un utilisateur dans la bdd.
 *
 * @author: Jérémy Thivet
 */
 
 namespace Models;
 USE \Illuminate\Database\Eloquent\Model;
 
 class Partenaire extends Model{
	 
	/**
     *   Définition des attributs du modèle
	 */
	 protected $table = 'partenaire';
	 protected $primaryKey = 'idUser';
	 public $timestamps = false;


     public function prestations(){
         return $this->belongsToMany('\Models\Prestation');
     }
	 
 }