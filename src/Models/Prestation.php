<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 11/02/16
 * Time: 09:44
 */

namespace Models;


use Illuminate\Database\Eloquent\Model;

class Prestation extends Model
{
    protected  $table='prestation';
    protected  $primaryKey='id';
    public  $timestamps=false;

    public function ptype(){
        return $this->belongsTo('\Models\Type','type');
    }

    public function partenaire(){
        return $this->belongsTo('\Models\Partenaire','idPartenaire');
    }

    public function pochettes(){
        return $this->belongsToMany('\Models\Pochette','pochetteprestation', 'idPrestation', 'idPochette')->withPivot('qte');
    }
}