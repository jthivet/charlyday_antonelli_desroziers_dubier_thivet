<?php
/**
 * Created by PhpStorm.
 * User: thivet4u
 * Date: 11/02/2016
 * Time: 10:48
 */

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Pochette extends Model
{

    protected  $table='pochette';
    protected  $primaryKey='idPochette';
    public  $timestamps=false;

    public function prestations(){
        return $this->belongsToMany('\Models\Prestation','pochetteprestation','idPochette', 'idPrestation')->withPivot('qte');
    }
}