<?php

namespace Controllers;

use Models\Pochette as Pochette;
use Models\Prestation;

class PochetteController extends AbstractController{
	
	public function creerPochette(){
		$_SESSION['uniqueId']=null;
		$this->app->render('pageCreationPochette.php', ['app' => $this->app]);
	}
	
	public function validerPochette()
	{
		$post = $this->app->request()->post();
		$erreurs[] = [];
		if(isset($post['nomPochette']) && isset($post['message'])) {
			if (empty($post['nomPochette'])) {
				$erreurs[] = "ERREUR: un nom doit être impérativement saisi.";
			} else {
				$NomPochette = filter_var($post['nomPochette'], FILTER_SANITIZE_STRING);
				$Message = filter_var($post['message'], FILTER_SANITIZE_STRING);
				$Pochette = new Pochette();
				$Pochette->uniqueId = uniqid();
				$Pochette->nom = $NomPochette;
				$Pochette->message = $Message;
				$Pochette->save();
				$_SESSION['uniqueId'] = $Pochette->uniqueId;
			}
		}else{
			$erreurs[] = "ERREUR: champ manquant";
		}

		$this->app->render('validationPochette.php', ['erreurs' => $erreurs, 'app' => $this->app]);
	}

	public function afficherPochette(){
		$this->app->render('affichagePochette.php', ['app' => $this->app]);
	}
	
	public function ajouterPrestation(){
		$post = $this->app->request()->post();
		$idPrestation = filter_var($post['idPrestation'], FILTER_SANITIZE_NUMBER_INT);
		if(!empty($_SESSION['uniqueId'])){
			$Pochette= Pochette::where('uniqueId',$_SESSION['uniqueId'])->first();
			$Prestations = $Pochette->prestations;
			$pres = null;
			if($Prestations != NULL) {
				foreach ($Prestations as $p) {
					if ($p->id == $idPrestation) {
						$pres = $p;
						break;
					}
				}
			}
			if($pres == null){
				$p = Prestation::find($idPrestation);
				$Pochette->prestations()->attach($p,array("qte"=> 1));
				$Pochette->save();
			}else{
				$pres->pivot->qte = $p->pivot->qte + 1;
				$pres->pivot->save();
			}
		}else{
			$this->app->redirectTo('creerpochette');
		}

		$this->app->render('affichagePochette.php', ['app' => $this->app]);
	}

	public function donationPochette($id)
	{
        $p = Pochette::where('uniqueId',$id)->get();
        if(count($p)==1)
		    $this->app->render('pageDonation.php', ['app' => $this->app,'id'=>$id]);
        else
            $this->app->redirectTo('home');
    }

	public function ajoutDonationPochette(){
		$post = $this->app->request()->post();
		$Pochette= Pochette::where('uniqueId', filter_var($post['idPochette'],FILTER_SANITIZE_STRING))->first();
		$Pochette->montantdons= $Pochette->montantdons+filter_var($post['don'],FILTER_SANITIZE_NUMBER_INT);
		$Pochette->save();
		$this->app->render('pageDonation.php', ['app' => $this->app,'id'=>filter_var($post['idPochette'],FILTER_SANITIZE_STRING)]);
	}

    public function lienCadeau($id){
        $Pochette= Pochette::where('uniqueId', $id)->first();
        $this->app->render('pageCadeau.php', ['app' => $this->app,'id'=>filter_var($id,FILTER_SANITIZE_STRING)]);
    }


}