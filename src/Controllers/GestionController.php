<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 11/02/16
 * Time: 13:23
 */

namespace Controllers;

use Models\Pochette;
use \Models\Prestation;
use Models\Type;

class GestionController extends AbstractController
{

    public function listerprestations(){

        if(!AuthentificationController::isConnect()){
            $this->app->redirect($this->app->urlFor('connexion'));
        }


        $prestations = Prestation::where('idPartenaire', $_SESSION['partenaire']->idUser)->get();

        $this->app->render('gestionlisterprestation.php', ['app' => $this->app, 'prestations' => $prestations]);
    }



    public function ajouter(){

        if(!AuthentificationController::isConnect()){
            $this->app->redirect($this->app->urlFor('connexion'));
        }

        $this->app->render('gestionajouterprestation.php', ['app' => $this->app]);

    }


    public function postajouter(){

        $post = $this->app->request()->post();

        $erreurs = array();

        if(isset($post['confirmation'])){
            $prestation = new Prestation();

            /** Début des tests **/

            // Test du nom
            $countprest = Prestation::where('nom', $post['prestname'])->count();
            if(empty($post['prestname']) || (!preg_match('#^[ a-z0-9éèàêâûîùïüë.\'-]{5,}$#i', $post['prestname']))) {

                $erreurs[] = "Nom de prestation incorrect, il doit contenir au moins cinq caractères et ne pas contenir de caractères spéciaux.";

            }
            if ($countprest>0) {

                $erreurs[] = "Le nom de la prestation est déjà utilisé.";

            } else {

                $prestname = filter_var($post['prestname'], FILTER_SANITIZE_STRING);

                $prestation->nom = $prestname;

            }

            // Test de la description
            if(empty($post['description']) || (strlen($post['description'])<10)) {

                $erreurs[] = "Description incorrecte, elle doit faire au moins 10 caractères.";

            } else {

                $descr = filter_var($post['description'], FILTER_SANITIZE_STRING);

                $prestation->descr = $descr;

            }


            // Test du type
            $type = Type::find($post['type'])->first();
            if(empty($post['type']) || empty($type)){

                $erreurs[] = "Type incorrect";

            } else {

                $prestation->ptype()->associate($type);

            }


            // Test du prix
            if(empty($post['prix']) || (!preg_match('#^[0-9]+$#', $post['prix'])) || $post['prix']<=0){

                $erreurs[] = "Le prix est incorrect, il doit être positif.";

            } else {

                $prestation->prix = $post['prix'];

            }


            // test de l'image
            if(empty($_FILES['image']) || $_FILES['image']['error'] !== 0){

                $erreurs[] = " Erreur pendant l'upload de l'image.";


            } else {


                $storage = new \Upload\Storage\FileSystem('web/img');
                $file = new \Upload\File('image', $storage);

                $nom = uniqid();
                $file->setName($nom);

                $prestation->img = $nom . '.' . $file->getExtension();


                $file->addValidations(array(
                    // Ensure file is of type "image/png"
                    new \Upload\Validation\Mimetype(array('image/png','image/jpg', 'image/gif', 'image/jpeg')),

                    // Ensure file is no larger than 5M (use "B", "K", M", or "G")
                    new \Upload\Validation\Size('1M')
                ));

                // Try to upload file
                try {
                    // Success!
                    $file->upload();
                } catch (\Exception $e) {

                    $erreurs[] = "L'image doit vérifier les conditions suivantes : <br>" .
                                "- Peser moins de 1 Mo.<br>" .
                                "- Avoir l'une des extensions suivantes : .jpg .gif .png";

                }

            }

            $prestation->idPartenaire = $_SESSION['partenaire']->idUser;


            if(count($erreurs)==0){
                $bool = $prestation->save();
                if(!$bool){
                    $erreurs[] = "Erreur pendant l'inserstion, contactez l'administrateur.";
                }
            }


        }

        $this->app->render('gestionajouterprestation.php', ['erreurs' => $erreurs, 'app' => $this->app]);
    }



    public function modifier($id)
    {

        if (!AuthentificationController::isConnect()) {
            $this->app->redirect($this->app->urlFor('connexion'));
        }

        $prestation = Prestation::find($id);

        if ($prestation->idPartenaire !== $_SESSION['partenaire']->idUser) {
            $this->app->redirect($this->app->urlFor('gestionlisteprestations'));
        }

        $this->app->render('gestionmodifierprestation.php', ['prestation' => $prestation, 'app' => $this->app]);
    }



    public function postmodifier($id){

        $post = $this->app->request()->post();
        $prestation = Prestation::find($id);

        $erreurs = array();

        if(isset($post['confirmation'])){
            /** Début des tests **/

            // Test du nom
            if(!(empty($post['prestname']))){

                $countprest = Prestation::where('nom', $post['prestname'])->count();

                if((!preg_match('#^[ a-z0-9éèàêâûîùïüë.\'-]{5,}$#i', $post['prestname']))) {

                    $erreurs[] = "Nom de prestation incorrect, il doit contenir au moins cinq caractères et ne pas contenir de caractères spéciaux.";

                }
                 else if ($countprest>0) {

                    $erreurs[] = "Le nom de la prestation est déjà utilisé.";

                 } else {

                     $prestname = filter_var($post['prestname'], FILTER_SANITIZE_STRING);

                     $prestation->nom = $prestname;
                 }
            }

            // Test de la description
            if(!empty($post['description'])){

                if (empty($post['description']) || (strlen($post['description']) < 10)) {

                    $erreurs[] = "Description incorrecte, elle doit faire au moins 10 caractères.";

                } else {

                    $descr = filter_var($post['description'], FILTER_SANITIZE_STRING);

                    $prestation->descr = $descr;

                }
            }


            // Test du type
            if($post['type']!==$prestation->type) {
                $type = Type::find($post['type'])->first();
                if (empty($type)) {

                    $erreurs[] = "Type incorrect";

                } else {

                    $prestation->ptype()->associate($type);

                }
            }


            // Test du prix
            if(!empty($post['prix'])) {

                if ((!preg_match('#^[0-9]+$#', $post['prix'])) || $post['prix'] <= 0) {

                    $erreurs[] = "Le prix est incorrect, il doit être positif.";

                } else {

                    $prestation->prix = $post['prix'];

                }
            }

            // test de l'image
            if(!empty($_FILES['image']['name'])) {

                if ($_FILES['image']['error'] !== 0) {

                    $erreurs[] = " Erreur pendant l'upload de l'image.";


                } else {


                    $storage = new \Upload\Storage\FileSystem('web/img');
                    $file = new \Upload\File('image', $storage);

                    $nom = uniqid();
                    $file->setName($nom);

                    $prestation->img = $nom . '.' . $file->getExtension();


                    $file->addValidations(array(
                        // Ensure file is of type "image/png"
                        new \Upload\Validation\Mimetype(array('image/png', 'image/jpg', 'image/gif', 'image/jpeg')),

                        // Ensure file is no larger than 5M (use "B", "K", M", or "G")
                        new \Upload\Validation\Size('1M')
                    ));

                    // Try to upload file
                    try {
                        // Success!
                        $file->upload();
                    } catch (\Exception $e) {

                        $erreurs[] = "L'image doit vérifier les conditions suivantes : <br>" .
                            "- Peser moins de 1 Mo.<br>" .
                            "- Avoir l'une des extensions suivantes : .jpg .gif .png";

                    }

                }
            }


            if(count($erreurs)==0){
                $bool = $prestation->save();
                if(!$bool){
                    $erreurs[] = "Erreur pendant l'insertion, contactez l'administrateur.";
                }
            }


        }

        $this->app->render('gestionmodifierprestation.php', ['prestation'=> $prestation, 'erreurs' => $erreurs, 'app' => $this->app]);
    }



    public function delete($id)
    {

        if (!AuthentificationController::isConnect()) {
            $this->app->redirect($this->app->urlFor('connexion'));
        }

        $prestation = Prestation::find($id);

        if ($prestation->idPartenaire !== $_SESSION['partenaire']->idUser) {
            $this->app->redirect($this->app->urlFor('gestionlisteprestations'));
        }

        $this->app->render('gestionsupprimerprestation.php', ['prestation' => $prestation, 'app' => $this->app]);
    }


    public function postdelete($id)
    {
        $prestation = Prestation::find($id);

        if ($prestation->idPartenaire !== $_SESSION['partenaire']->idUser) {
            $this->app->redirect($this->app->urlFor('gestionlisteprestations'));
        }

        $prestation->delete();


        $this->app->redirect($this->app->urlFor('gestionlisteprestations'));

    }



    public function listercommandes(){

        if (!AuthentificationController::isConnect()) {
            $this->app->redirect($this->app->urlFor('connexion'));
        }

        $prestations = Prestation::where("idPartenaire", $_SESSION['partenaire']->idUser)->get();

        $pochettes = array();

        foreach($prestations as $prestation){


           $prest =  $prestation->pochettes;

            foreach($prest as $val){
                $pochettes[] = $val;
            }

        }

        $this->app->render('gestionlistecommande.php', ['pochettes' => $pochettes, 'app' => $this->app]);


    }


}