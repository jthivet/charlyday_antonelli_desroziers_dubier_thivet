<?php

namespace Controllers;

use Models\Prestation as Prestation;
use Models\Type;

class PrestationsController extends AbstractController{
	
	public function afficherPrestations($p = 1)
	{
        $get = $this->app->request()->get();
        if(!isset($get['filtreType']) || empty($get['filtreType']))
            $filtreType = 0;
        else
            $filtreType = $get['filtreType'];

        if(isset($get['desc']) && $get['desc'])
            $desc = true;
        else
            $desc = false;
		$nbDisplay = 6;

        $type = Type::find($filtreType);
        if($type != NULL)
            $nbRows = $type->prestations->count();
        else
            $nbRows = Prestation::all()->count();

        $start = $p * $nbDisplay - $nbDisplay;
        $pagination = (int)ceil($nbRows/$nbDisplay);


        if($desc)
            if($type==NULL)
                $prestations = Prestation::take($nbDisplay)->skip($start)->orderBy('prix','DESC')->get();
            else
                $prestations = $type->prestations()->take($nbDisplay)->skip($start)->orderBy('prix','DESC')->get();
        else
            if($type==NULL)
                $prestations = Prestation::take($nbDisplay)->skip($start)->orderBy('prix','ASC')->get();
            else
                $prestations = $type->prestations()->take($nbDisplay)->skip($start)->orderBy('prix','ASC')->get();

        $types = Type::all();
        $this->app->render('listeprestations.php',
            [
                'desc'=>$desc,
                'filtreType'=>$filtreType,
                'types'=>$types ,
                'prestations' => $prestations,
                'app' => $this->app,
                'pagination'=>$pagination,
                'p'=>$p
            ]);
	}
	
	public function afficherPrestation($id)
	{
		$prestation = Prestation::find($id);
		$this->app->render('detailPrestation.php', ['prestation' => $prestation, 'app' => $this->app]);
	}

    public function voterPrestation(){
        $post = $this->app->request()->post();
        $Prestation= Prestation::where('id', filter_var($post['idPrestation'],FILTER_SANITIZE_STRING))->first();
        $Note= filter_var($post['note'],FILTER_SANITIZE_STRING);
        if($Note<0)
            $Note=0;
        if($Note>5)
            $Note=5;
        $Prestation->cumulNotes+=$Note;
        $Prestation->nbNotes+=1;
        $Prestation->save();
        $this->app->render('detailPrestation.php', ['prestation' => $Prestation, 'app' => $this->app]);
    }
	
	


}