<?php

namespace Controllers;

use \Models\Partenaire;

class AuthentificationController extends AbstractController{



	public function inscription(){

		if(AuthentificationController::isConnect()){

			$this->app->redirect($this->app->urlFor('home'));

		}

		$this->app->render('inscription.php', ['app' => $this->app]);

	}



	/**
	 * Vérifie le formulaire et retourne des erreurs en cas de problème.
	 */
	public function postinscription(){

		$post = $this->app->request()->post();

		$erreurs = array();
		if(isset($post['confirmation'])){
			$partenaire = new Partenaire();

			/** Début des tests **/

			 // Test du username
			$countusername = Partenaire::where('username', $post['username'])->count();
			if(empty($post['username']) || (!preg_match('#^[a-z0-9éèàêâûîùïüë-]{3,}$#i', $post['username']))) {

				$erreurs[] = "Nom d'utilisateur incorrect, il doit contenir au moins trois caractères et ne pas contenir de caractères spéciaux.";

			}
			if ($countusername>0) {

				$erreurs[] = "Le nom d'utilisateur est déjà utilisé.";

			} else {

				$username = filter_var($post['username'], FILTER_SANITIZE_STRING);

				$partenaire->username = $username;

			}

			// Test du nom de partenaire
			$countusername = Partenaire::where('nom', $post['nom'])->count();
			if(empty($post['nom']) || (!preg_match('#^[ a-z0-9éèàêâûîùï.ü\'ë-]{3,}$#i', $post['nom']))) {

				$erreurs[] = "Nom de partenaire incorrect, il doit contenir au moins trois caractères et ne pas contenir de caractères spéciaux.";

			}
			if ($countusername>0) {

				$erreurs[] = "Le nom de partenaire est déjà utilisé.";

			} else {

				$partenairename = filter_var($post['nom'], FILTER_SANITIZE_STRING);

				$partenaire->nom = $partenairename;

			}


			 // Test du mot de passe.
			if(empty($post['pass']) || empty($post['repass'])){

				$erreurs[] = " Mot de passe non renseigné";

			} else if ($post['pass'] !== $post['repass']) {

				$erreurs[] = "Les mots de passe renseignés doivent être identiques.";

			} else if (!preg_match('#^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$#', $post['pass'])){

				$erreurs[] = "Le mot de passe n'est pas assez sécurisé, il doit vérifier :<br>
				- Taille minimum de 6 caractères.<br>
				- Contenir au moins une majuscule.<br>
				- Contenir au moins un chiffre.<br>";

			} else {

				$hash = password_hash($post['pass'], PASSWORD_DEFAULT, array('cost'=> 12));

				$partenaire->motdepasse = $hash;

			}

			if(count($erreurs)==0){
				$bool = $partenaire->save();
				if(!$bool){
					$erreurs[] = "Erreur pendant l'inscription, contactez l'administrateur.";
				}
			}

	
		}

		$this->app->render('inscription.php', ['erreurs' => $erreurs, 'app' => $this->app]);
		
	}




	public function connexion() {

		if(AuthentificationController::isConnect()){

			$this->app->redirect($this->app->urlFor('home'));

		}

		$this->app->render('connexion.php', ['app' => $this->app]);
	}


	public function postconnexion()
	{

		$post = $this->app->request()->post();
		$erreurs = array();

		if (isset($post['confirmation'])) {

			$partenaire = Partenaire::where('username', $post['username'])->first();

			if (empty($partenaire)) {

				$erreurs[] = "Aucun utilisateur trouvé avec cet id.";

			} else {

				$hash = $partenaire->motdepasse;

				if (!password_verify($post['pass'], $hash)) {

					$erreurs[] = "Mauvais couple identifiant/mot de passe, veuillez réessayer.";

				}
			}


			if (empty($erreurs)) {

				unset($_SESSION['partenaire']);
				session_regenerate_id();

				$_SESSION['partenaire'] = $partenaire;
			}
		}

		$this->app->render('connexion.php', ['erreurs' => $erreurs, 'app' => $this->app]);
	}

	public function deconnexion(){

		if(!AuthentificationController::isConnect()){

			$this->app->redirect($this->app->urlFor('connexion'));

		}


		unset($_SESSION['partenaire']);

		$this->app->redirect($this->app->urlFor('home'));

	}


	public static function isConnect(){

		return !empty($_SESSION['partenaire']);


	}
	


}