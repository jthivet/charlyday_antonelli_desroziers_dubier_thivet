<?php
/**
  * File:  Router.php
  * Creation Date: 11/02/2016
  * Auteur : Jérémy
  * Description : Routeur dynamique permettant d'alléger la création de routes sur Slim.
  */

 namespace Controllers;

 class Router {

 	/**
	* Référence vers Slim.
 	*/
 	private $app;


 	public function __construct($app){

 		$this->app = $app;
 	}

 	/**
 	 * Fonction permettant de traiter l'action en cours
 	 * @param url url de la page sur laquelle on se trouve
 	 * @param action action de la page découpée avec un '@' de cette façon : nomducontroller@nommethodecontroller
 	 * @param method methode d'accès à la page (get, post)
 	 * @return une route du framework Slim 
 	 */
 	public function call($url, $action, $method){

 		return $this->app->$method($url, function () use ($action){

 			$action = explode('@',$action);

 			$controllername = '\Controllers\\' . $action[0] . 'Controller';
 			$method			= $action[1];

 			if(class_exists($controllername)){
				$controller = new $controllername();
			}else{
				$controller = new PublicController();
				$method = 'home';
			}

			// On appelle la methode du controller avec d'éventuels paramètres.
			call_user_func_array([$controller, $method], func_get_args());
 		});
 	}




 	/**
 	 * Fonction permettant de traiter l'action en cours en cas de get
 	 * @param url url de la page sur laquelle on se trouve
 	 * @param action action de la page découpée avec un '@' de cette façon : nomducontroller@nommethodecontroller
 	 * @return une route du framework Slim 
 	 */
 	public function get($url, $action){

 		return $this->call($url, $action, 'get');
 	}



	/**
 	 * Fonction permettant de traiter l'action en cours en cas de post
 	 * @param url url de la page sur laquelle on se trouve
 	 * @param action action de la page découpée avec un '@' de cette façon : nomducontroller@nommethodecontroller
 	 * @return une route du framework Slim 
 	 */
 	public function post($url, $action){

 		return $this->call($url, $action, 'post');
 	}

 }

