<?php

/**
 * File:  AbstractController.php
 * Creation Date: 11/01/2016
 * descrption: Controller abstrait, permet d'initialiser un contrôleur et prenant en paramètre l'objet $app de Slim
 *
 * @author: Jérémy Thivet
 */

namespace Controllers;

abstract class AbstractController {


   /**
    * App de Slim
    */
   protected $app;

   /**
 	* Constructeur de la classe.
 	* @param app app de Slim
 	*/
 	function __construct(\Slim\Slim $app = null) {

 		if(empty($app)){
			$app = \Slim\Slim::getInstance();
		}
		$this->app = $app;

 	}
	


 }